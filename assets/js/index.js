$(".title").prepend('<img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABEAAAAQCAYAAADwMZRfAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAANLSURBVDhPLZNNaFxVFMd/77737puPzOSjNlMijWmnQUkRLFkEbLBaTFNcCIqUrrJQUBBcdFMouHAhFHGRvahdKFjBTamCEBdVqjgVUWlpwUTbhDZN0mkmM5lkZt7HvZ734oHLvfe9e875n///HGe5cdv+tPwDMf0Ya9m2mnYcECeGpikRGUMr8diOHTbR1CNFHZ9Hscb0Yr6YPIz74tzoB76nAZfYuCg5eY6DkVPPxKzt7OC7DkppYkliHEVkHXAsHQlWa7ZRRc/Dw6JVJM7gOzG7cYvHnS2mK6N8deosZ6pHKbigTYi2Ef2OQTsJrgQqe5LYUxZflqsSwmSTnAR9tXqSj194S/ZJNrttrq8ucafxgJknR5go59nothgSn7wbkwhi58fFj2xkfZqhQ1EPMTs+JwXBtZUbVAcPcbC0n0edNvvzfdn31N6u3eD79TaxzjMQlPF8FQqJin2FQU4cmuPyrU+4v91Cuf0cGapmTmmAX9fucvHP38jpfuanjgtP//LtRpM+QaMcHDwJZJKdzOHE2CmBCS+NHefSX19y9Z+feXPhMxabDa6cPsPXJ2cZKfaJahFl4c8XCTztdolEUlfISi1JQkLT4tjwRLbOX5vn85lz2b/Urq4scWXlHvW0fOGvJH7KFdl8OXhCbGYib+AIMrt3zfl+ttfWljh3/RtGC318Ov0yxwb6CARNThRS2k+liqSoPSSO7ck9FNH3TBNm+9SBI8xPv0HR97hQW2Cxtc2gawhU2lHyOhBytIqzx/uKVZ4afJrEdLN7TupO7ff1v3n/l8vcfLzOxakZnhUk0oMUpCGVlqzpxdguq62b0id5To+/g3ZzmXPamalNVsb58PmzvHb4qARa5f5OQxrQUBQRPCXt4gukwMbc27jErdWEkYFZJiqvpNIJX1EWRKKxsFzju7t36JGTssrkxE/LnDjLD9+zofFEkRy9WLFrCoSxpS5dOVx6jnqnRTE4yB/rtwn0sDBXoJdYtpISPZsI8rLMlW1Ijr2Zcf9XRrjjicIBdntrQlrIVucBlUKFkquEyFgEdAVFV2ZOKBUh1NDAu3LZEuiROEvzyCBmkjtdySLMy9QGbk9gp2XH8j19k1CQd2HS4fWxZ/gPnTxMiu9u4SgAAAAASUVORK5CYII=" style="display:inline"/> ');

$("#result .edit_mark").append('<img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABkAAAAYCAYAAAAPtVbGAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAG7SURBVEhL7ZXLTsJAFIb/mcKCq7FIgkEFN/gOJiZojOJOWPGeKDsTxEsxWrm0G7bGRI1GN2IVinimVAPEamviji9pZjLnTP4z55KyAYF/htvrvzIV+aK8X8Zh5Qj4obLeRd7tlagcVNBut6FpLVSrVZimaVvG8SYioqUbvW4Ptzd3yKxkkEjMWyZd03F9dW3tJ3En8pkKNlxOT06xVyrhudPBTi6HcCiEVDqNufjc0GECd3MiUmSHc1w9hq7r1p77fFhbXcXScgqccwQCAet8Encvsb2UE8US8Pv94BLHq/GKpqYhRC9xEhC4nviaUkP9sg4fRS8u9Pt9qkcC+d3dX0N1No9IX5xdoNVsgUkMnDHqoh4WkwvYzm25ysX3LiMC6rmKeqNOZwMSkNAjgWQyiex6FsFgcMzXie9FRBfR12w0oaoqzL5JtWfodt8Qjc4gJzoqGh4K2B33E441EQKKooAxDok6RwxaOBJBsViEzy/ZXu5wzGiHZkCEKWogihyPx1Eo5D0LCBxr0qWpNowXvBgGYnIMG5sbCIaoBn/AMV0P9w94fHqkVEmYlWchy7Jt8c70z+iJqYgHgA//haKiOlCzGwAAAABJRU5ErkJggg==" style="display:inline"/>');


$(".names li, .names li p, .operation .upperalpha li, .operation .upperalpha li p").css({
    "font-weight":"800",
    "color":"#0000a5"
});

$(".operation .paragraph, .operation .paragraph p, .exampleblock li, .exampleblock li p").css({
    "color": "black",
    "font-weight": "normal"
});


function containerMaker(str) {
    s = str.replace(/<h6(.*)<\/h6>/gimu, '<h6$1</h6><div class="H6">')
        .replace(/<h5(.*)<\/h5>/gimu, '<h5$1</h5><div class="H5">')
        .replace(/<h4(.*)<\/h4>/gimu, '<h4$1</h4><div class="H4">')
        .replace(/<h3(.*)<\/h3>/gimu, '<h3$1</h3><div class="H3">')
        .replace(/<h2(.*)<\/h2>/gimu, '<h2$1</h2><div class="H2">')
        .replace(/<h1(.*)<\/h1>/gimu, '<h1$1</h1><div class="H1">');
    let hArray = [];
    for (let h = 0; h < 6; h++) {
        let i = 0;
        while (i < s.length) {
            let index = s.indexOf('<h' + (h + 1), i);
            if (index == -1) {
                break;
            }
            let obj = {
                head: h + 1,
                index: index
            };
            hArray.push(obj);
            i = index + 1;

        }
    }
    hArray.sort(function (x, y) {
        return x.index - y.index;
    })
    let divCloseIndexs = [];
    for (let x = 0; x <= hArray.length; x++) {
        for (next = x + 1; next < hArray.length; next++) {
            if (hArray[x].head - hArray[next].head < 0) {
                if (next == hArray.length - 1) {
                    divCloseIndexs.push(s.length);
                }
            } else {
                divCloseIndexs.push(hArray[next].index);
                break;
            }
        }
    }
    divCloseIndexs = divCloseIndexs.sort(function (x, y) {
        return x - y;
    });
    let addStrNum = 0;
    for (let x = 0; x <= divCloseIndexs.length; x++) {
        s = strIns(s, divCloseIndexs[x] + addStrNum, '</div>');
        addStrNum = addStrNum + 6;
    }
    return s;
};
